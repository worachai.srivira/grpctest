package services

import (
	context "context"
	"encoding/json"
	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"golang.org/x/crypto/bcrypt"
	_ "golang.org/x/crypto/bcrypt"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"grpcTest/db"
	"strings"
)

type testApiServer struct {
	//UnimplementedTestApiServer
}

func NewTestApiServer() TestApiServer {
	return testApiServer{}
}

func (testApiServer) mustEmbedUnimplementedTestApiServer() {}

type User struct {
	UserName string
	Password string
	FName    string
	LName    string
	Phone    string
	Email    string
	Address  string
}

type UserJson struct {
	UserName string
	FName    string
	LName    string
	Phone    string
	Email    string
	Address  string
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password string, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func getAccessToken(ctx context.Context) (string, error) {
	md, err := metadata.FromIncomingContext(ctx)
	if !err {
		return "", status.Errorf(codes.DataLoss, "failed to get metadata")
	}
	xrid := md["accesstoken"]
	if len(xrid) == 0 {
		return "", status.Errorf(codes.InvalidArgument, "missing 'accessToken' header")
	}
	if strings.TrimSpace(xrid[0]) == "" {
		return "", status.Errorf(codes.InvalidArgument, "empty 'accessToken' header")
	}
	return xrid[0], nil
}

func (testApiServer) Register(ctx context.Context, req *RegisterRequest) (*RegisterResponse, error) {

	password := req.Password
	hash, errHash := HashPassword(password)
	if errHash != nil {
		res := RegisterResponse{
			Status:  "Fail",
			Message: "errHash",
		}
		return &res, nil

	}
	coll := db.MongodbClient.Database("exlogin").Collection("users")
	var result bson.M
	errChkUserName := coll.FindOne(context.TODO(), bson.D{{"userName", req.UserName}}).Decode(&result)
	if errChkUserName == nil {
		res := RegisterResponse{
			Status:  "Fail",
			Message: "Username is dupicate",
		}
		return &res, nil

	}

	doc := bson.D{
		{"userName", req.UserName},
		{"password", hash},
		{"fName", req.FName},
		{"lName", req.LName},
		{"phone", req.Phone},
		{"email", req.Email},
		{"address", req.Address},
	}
	_, insertErr := coll.InsertOne(context.TODO(), doc)
	if insertErr != nil {
		res := RegisterResponse{
			Status: "Fail",
		}
		return &res, nil
	}
	res := RegisterResponse{
		Status:  "Success",
		Message: "Register Success",
	}

	return &res, nil
}

func (testApiServer) Login(ctx context.Context, req *LoginRequest) (*LoginResponse, error) {

	coll := db.MongodbClient.Database("exlogin").Collection("users")

	var result User

	errChkUserName := coll.FindOne(context.TODO(), bson.D{{"userName", req.UserName}}).Decode(&result)
	if errChkUserName != nil {
		resLogin := LoginResponse{
			Status:  "Fail",
			Message: "Incorrect Username",
		}
		return &resLogin, nil
	}
	chkPw := result.Password
	match := CheckPasswordHash(req.Password, chkPw)
	if match != true {
		resLogin := LoginResponse{
			Status:  "Fail",
			Message: "Incorrect Password",
		}
		return &resLogin, nil
	}
	token := uuid.New()

	var resultJson UserJson
	errJson := coll.FindOne(context.TODO(), bson.D{{"userName", req.UserName}}).Decode(&resultJson)
	if errJson != nil {
		resLogin := LoginResponse{
			Status:  "Fail",
			Message: errJson.Error(),
		}
		return &resLogin, nil
	}

	jsonData, errJson := json.MarshalIndent(resultJson, "", "    ")
	if errJson != nil {
		resLogin := LoginResponse{
			Status:  "Fail",
			Message: errJson.Error(),
		}
		return &resLogin, nil
	}

	err := db.RedisClient.Set(ctx, token.String(), jsonData, 0).Err()
	if err != nil {
		resLogin := LoginResponse{
			Status:  "Fail",
			Message: err.Error(),
		}
		return &resLogin, nil
	}

	resLogin := LoginResponse{
		Status:      "Success",
		AccessToken: token.String(),
		Message:     "Login Success",
	}
	return &resLogin, nil
}

func (testApiServer) ValidateToken(ctx context.Context, req *ValidateTokenRequest) (*ValidateTokenResponse, error) {
	token, errToken := getAccessToken(ctx)
	if errToken != nil {
		resValidate := ValidateTokenResponse{
			Status:  "Fail",
			Message: errToken.Error(),
		}
		return &resValidate, nil
	}

	_, err := db.RedisClient.Get(ctx, token).Result()

	if err != nil {
		resValidate := ValidateTokenResponse{
			Status:  "Fail",
			Message: "Token is inActive",
		}
		return &resValidate, nil
	}
	resValidate := ValidateTokenResponse{
		Status:  "Success",
		Message: "Token is Active",
	}
	return &resValidate, nil
}

func (testApiServer) Logout(ctx context.Context, req *LogoutRequest) (*LogoutResponse, error) {
	token, errToken := getAccessToken(ctx)
	if errToken != nil {
		resLogout := LogoutResponse{
			Status:  "Fail",
			Message: errToken.Error(),
		}
		return &resLogout, nil
	}

	_, err := db.RedisClient.Get(ctx, token).Result()
	if err != nil {
		resLogout := LogoutResponse{
			Status:  "Fail",
			Message: "UNAUTHORIZED TOKEN EXPIRED",
		}
		return &resLogout, nil
	}

	_, err = db.RedisClient.Del(ctx, token).Result()
	if err != nil {
		resLogout := LogoutResponse{
			Status:  "Fail",
			Message: err.Error(),
		}
		return &resLogout, nil
	}
	resLogout := LogoutResponse{
		Status:  "Success",
		Message: "Logout Success",
	}
	return &resLogout, nil
}

func (testApiServer) GetAllUser(ctx context.Context, req *GetAllUserRequest) (*GetAllUserResponse, error) {

	coll := db.MongodbClient.Database("exlogin").Collection("users")
	cursor, err := coll.Find(ctx, bson.M{})
	if err != nil {
		resGetAllUser := GetAllUserResponse{
			Status:  "Fail",
			Message: err.Error(),
		}
		return &resGetAllUser, nil
	}
	var listUser []*UserResponse

	defer cursor.Close(ctx)

	for cursor.Next(ctx) {
		var user UserJson
		if err = cursor.Decode(&user); err != nil {
			resGetAllUser := GetAllUserResponse{
				Status:  "Fail",
				Message: err.Error(),
			}
			return &resGetAllUser, nil
		}
		listUser = append(
			listUser, &UserResponse{
				UserName: user.UserName,
				FName:    user.FName,
				LName:    user.LName,
				Phone:    user.Phone,
				Email:    user.Email,
				Address:  user.Address,
			})
	}
	resGetAllUser := GetAllUserResponse{
		Status:  "Success",
		Message: "Get All User",
		AllUser: listUser,
	}
	return &resGetAllUser, nil

}
