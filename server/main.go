package main

import (
	"fmt"
	"google.golang.org/grpc"
	"grpcTest/db"
	"grpcTest/services"
	"log"
	"net"
)

func main() {
	s := grpc.NewServer()
	listener, err := net.Listen("tcp", ":50051")
	if err != nil {
		log.Fatal(err)
	}
	db.ConnectMongodb()
	db.ConnectRedis()

	services.RegisterTestApiServer(s, services.NewTestApiServer())

	fmt.Println("gRPC server listening on port 50051")
	err = s.Serve(listener)
	if err != nil {
		log.Fatal(err)
	}
}
